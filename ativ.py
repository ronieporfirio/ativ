# coding=utf-8
# https://lazka.github.io/pgi-docs/Gtk-3.0/classes.html

from __future__ import print_function

import os
import sys
import gi
import datetime
import os.path
import argparse

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

gi.require_version('Gtk','3.0')
from gi.repository import Gtk,Gdk,Pango,GLib

print("")

clNoColor='\033[0m'
clBlack='\033[0;30m'
clRed='\033[0;31m'
clGreen='\033[0;32m'
clOrange='\033[0;33m'
clBlue='\033[0;34m'
clPurple='\033[0;35m'
clCyan='\033[0;36m'
clLight_Gray='\033[0;37m'
clDark_Gray='\033[1;30m'
clLight_Red='\033[1;31m'
clLight_Green='\033[1;32m'
clYellow='\033[1;33m'
clLight_Blue='\033[1;34m'
clLight_Purple='\033[1;35m'
clLight_Cyan='\033[1;36m'
clWhite='\033[1;37m'

def printCor(clColor,arg):
    print(clColor + arg + clNoColor)

def printL(arg):
    print(clOrange + arg + clNoColor)

def printR(arg):
    print(clRed + arg + clNoColor)

def printA(arg):
    print(clLight_Blue + arg + clNoColor)

def printC(arg):
    print(clLight_Cyan + arg + clNoColor)

def printY(arg):
    print(clYellow + arg + clNoColor)


# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# Os Strings de Pasta Dir nunca terão a última /
strSep = os.path.sep
strPathDir = "/home/ronie/prog/ativ"
strGladeFullFilename = strPathDir  + strSep +  "ativ.glade"
strTokenFullFilename = strPathDir + strSep +  "token.json"
strCredentialsFullFilename = strPathDir + strSep +  "credentials.json"


ap = argparse.ArgumentParser()
ap.add_argument("-d", "--developer", required=False,help="Executar como desenvolvedor")
args =  vars(ap.parse_args())
if args['developer'] != None:
    print("")
    printR('##### Modo Desenvolvedor #####')
    print("")
    booDev = True
else:
    print("")
    printY('##### Modo Usuário #####')
    print("")
    booDev = False

class Manipulador:
    def __init__(self):
        self.Stack: Gtk.Stack = Builder.get_object("main_stack")
        self.SPREADSHEET_R_ID = '1lRyEuExE99-1sIK3fykWCUC1BDXm5yWwCh9zcSPw60M'
        if (not booDev):
            self.autentica()
        self.iniciaCalendario()
        if (not booDev):
            self.iniciaProjetos()
        self.iniciaTextviews()

        # https://lazka.github.io/pgi-docs/Gtk-3.0/classes/Calendar.html#Gtk.Calendar.select_month

    def limparTela(self):
        self.setTextviewIDText('txtAtividades','')
        self.setTextviewIDText('txtObservacoes','')
        self.setTextviewIDText('txtEvidencias','')
        Builder.get_object("checkRelatar").set_active(False)


    def iniciaTextviews(self):
        self.iniciaTextviewID('txtAtividades')
        self.iniciaTextviewID('txtObservacoes')
        self.iniciaTextviewID('txtEvidencias')

    def iniciaTextviewID(self,strTextviewID):
        objTextview = Builder.get_object(strTextviewID)
        objBuffer = objTextview.get_buffer()

        # Inicializações de novos objetos antes de conectar (ocorre apenas na primera vez)
        objBuffer.listCheck = []
        objBuffer.numLinhaAnterior = 0
        objBuffer.numLinhaAtual = 0
        objBuffer.tag_item = objBuffer.create_tag("item", weight=Pango.Weight.BOLD, foreground="yellow")

        objBuffer.connect("end_user_action",self.on_buffer_end_user_action)

    def iniciaCalendario(self):
        today = datetime.datetime.today()
        self.cbCalendario: Gtk.Calendar = Builder.get_object("cdCalendario")
        self.cbCalendario.select_month(int(today.strftime("%-m"))-1, int(today.strftime("%Y")))
        self.cbCalendario.select_day(int(today.strftime("%-d")))


        # https://www.darkartistry.com/2018/07/gtk3-calendar-dialog-example-in-python/

        self.cbCalendario.set_property("show-details",False)
        self.cbCalendario.set_detail_height_rows(5)
        self.cbCalendario.set_detail_width_chars(10)
        self.cbCalendario.set_detail_func(self.meuCalendarDetailFunc)

        # Muito legal
        # Com isso posso no Diario resumir os ## e mostrar como Tooltip


    def meuCalendarDetailFunc(self,calendar, year, month, day):
        return '{}/{}/{}\nAlguma Coisa\n Outra Linha'.format(day,month,year)


    def descobreProximaLinha(self):
        self.RANGE_DADOS = 'Registro!A1:A1947'
        result = self.sheet.values().get(spreadsheetId=self.SPREADSHEET_R_ID,range=self.RANGE_DADOS).execute()
        totalCells = len(result['values'])
        self.numProxLinha = totalCells + 1

    def iniciaProjetos(self):
        # Prepara ComboBox de Projetos
        self.cbProjetos: Gtk.ComboBoxText = Builder.get_object("cbProjetos")
        self.RANGE_DADOS = 'Atividades!A2:B21'
        result = self.sheet.values().get(spreadsheetId=self.SPREADSHEET_R_ID,range=self.RANGE_DADOS).execute()
        self.dictProjetos = {}
        for listProjeto in result['values']:
            self.dictProjetos[listProjeto[0]] = listProjeto[1]
            self.cbProjetos.append(listProjeto[0],listProjeto[1])

        # Descobre próxima linha
        self.descobreProximaLinha()

        # Pega último projeto inserido e ativa no combobox
        result = self.sheet.values().get(spreadsheetId=self.SPREADSHEET_R_ID,range='Registro!F' + str(self.numProxLinha - 1)).execute()
        self.cbProjetos.set_active(list(self.dictProjetos.keys()).index(result['values'][0][0]))

    def on_btnAdicionar_clicked(self,button):
        if (not booDev):
            # Descobre próxima linha
            self.descobreProximaLinha()

            # Pega data do calendário e insere somente a data
            y,m,d = self.cbCalendario.get_date()
            m = m + 1 # O mes sempre vem com offset em 0
            dtData = datetime.date(y,m,d)
            strData = dtData.strftime("%d/%m/%Y")

            # Insere a data
            RANGE_TO_ADD = 'Registro!A' + str(self.numProxLinha)
            valores_to_add = [[strData]]
            result = self.sheet.values().update(spreadsheetId=self.SPREADSHEET_R_ID,range=RANGE_TO_ADD, valueInputOption="USER_ENTERED",body={"values":valores_to_add}).execute()

            #### Insere demais dados

            # Projeto
            strProjetoEscolhido = self.cbProjetos.get_active_text()
            key_list = list(self.dictProjetos.keys())
            val_list = list(self.dictProjetos.values())
            pos = val_list.index(strProjetoEscolhido)
            strProjetoID = key_list[pos]

            # Atividade
            strAtividades = self.getTextViewText(Builder.get_object("txtAtividades"))

            # Relatar
            checkRelatar = Builder.get_object("checkRelatar").get_active()
            if (checkRelatar):
                strRelatar = 'SIM'
            else:
                strRelatar = '';

            # Observações
            strObservacoes = self.getTextViewText(Builder.get_object("txtObservacoes"))

            # Relatado
            strRelatado = ''

            #Evidências
            strEvidencias = self.getTextViewText(Builder.get_object("txtEvidencias"))

            # Insere
            RANGE_TO_ADD = 'Registro!F' + str(self.numProxLinha)
            valores_to_add = [[strProjetoID,strAtividades,strRelatar,strObservacoes,strRelatado,strEvidencias]]
            result = self.sheet.values().update(spreadsheetId=self.SPREADSHEET_R_ID,range=RANGE_TO_ADD, valueInputOption="USER_ENTERED",body={"values":valores_to_add}).execute()
            self.msgConfirmacao('Informação','Registro Adicionado!','dialog-emblem-default')
            self.limparTela()

    # Funções Auxiliares
    def on_paste_done(self,text_buffer, clipboard):
        # https://python-gtk-3-tutorial.readthedocs.io/en/latest/clipboard.html
        clipTexto = clipboard.wait_for_text()
        novoTexto = clipTexto.replace("\n"," ")
        novoTexto = novoTexto.replace("  "," ")
        novoTexto = novoTexto.replace("   "," ")
        novoTexto = novoTexto.replace("    "," ")
        novoTexto = novoTexto.replace("     "," ")
        numChar = len(clipTexto)
        m = text_buffer.get_insert()
        iter_depois = text_buffer.get_iter_at_mark(m)
        iter_antes = text_buffer.get_iter_at_mark(m)
        iter_antes.backward_chars(numChar)
        # text_buffer.select_range(iter_antes,iter_depois)
        text_buffer.delete(iter_antes,iter_depois)
        text_buffer.insert_at_cursor(novoTexto,-1)

    def getTextViewText(self,textview):
        buffer = textview.get_buffer()
        startIter, endIter = buffer.get_bounds()
        text = buffer.get_text(startIter, endIter, False)
        return text

    def setTextviewText(self,textview,text):
        buffer = textview.get_buffer()
        startIter, endIter = buffer.get_bounds()
        buffer.delete(startIter, endIter)
        buffer.insert_at_cursor(text + '\0',-1)

    def setTextviewIDText(self,strTextviewID,text):
        objTextview = Builder.get_object(strTextviewID)
        objBuffer = objTextview.get_buffer()
        startIter, endIter = objBuffer.get_bounds()
        objBuffer.delete(startIter, endIter)
        objBuffer.insert_at_cursor(text + '\0',-1)

    def msgConfirmacao(self, param, param1, param2):
        mensagem: Gtk.MessageDialog = Builder.get_object("msgConfirmacao")
        mensagem.props.text = param
        mensagem.props.secondary_text = param1
        mensagem.props.icon_name = param2
        mensagem.show_all()
        mensagem.run()
        mensagem.hide()

    def autentica(self):
        # Essa é a parte da autenticação
        creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists(strTokenFullFilename):
            creds = Credentials.from_authorized_user_file(strTokenFullFilename, SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    strCredentialsFullFilename, SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open(strTokenFullFilename, 'w') as token:
                token.write(creds.to_json())
        self.service = build('sheets', 'v4', credentials=creds)
        self.sheet = self.service.spreadsheets()


    def sair(self):
        printL('Saindo...')
        Gtk.main_quit()

    def on_main_window_destroy(self, Window):
        self.sair()

    def on_btnSair_clicked(self,button):
        self.sair()



    def getItersFromLine(self,textbuffer,numLinha):
        numLinhaTotal = textbuffer.get_line_count()
        iterLineStart = textbuffer.get_iter_at_line(numLinha)
        if ((numLinha + 1) != numLinhaTotal):
            iterLineEnd = textbuffer.get_iter_at_line(numLinha+1)
            iterLineEnd.backward_char()
        else:
            iterLineEnd = textbuffer.get_end_iter()
        strLinhaTexto = textbuffer.get_text(iterLineStart,iterLineEnd,False)
        return iterLineStart,iterLineEnd,strLinhaTexto


    def on_buffer_end_user_action(self,text_buffer):
        buf = text_buffer
        #print("")
        iterStart = buf.get_start_iter()
        iterEnd = buf.get_end_iter()

        # Total de Linhas e Posicao do Cursor
        numLinhaTotal = buf.get_line_count()
        numPosCursor = buf.props.cursor_position
        #print("Total de Linhas = {}".format(numLinhaTotal))
        #print("Pos Cursor = {}".format(numPosCursor))


        # Linha Atual
        m = buf.get_insert()
        i = buf.get_iter_at_mark(m)
        buf.numLinhaAtual = i.get_line()
        #print("Linha Anterior = {}".format(buf.numLinhaAnterior))
        #print("Linha Atual = {}".format(buf.numLinhaAtual))

        # Testa se trocou de linha
        if buf.numLinhaAtual != buf.numLinhaAnterior:
            #print('Trocou de linha')
            # Aqui é o evento em que verifico se essa linha está na lista
            iterLineStart,iterLineEnd,strLinhaTexto = self.getItersFromLine(buf, buf.numLinhaAnterior)
            if strLinhaTexto != '':
                if not (buf.numLinhaAnterior in buf.listCheck):
                    #print('Ainda não foi feito insert')
                    buf.listCheck.append(buf.numLinhaAnterior)

                    # Insere "- "
                    iterLineStart,iterLineEnd,strLinhaTexto = self.getItersFromLine(buf, buf.numLinhaAnterior)
                    buf.insert(iterLineStart,'- ')

                    # Insere ";"
                    iterLineStart,iterLineEnd,strLinhaTexto = self.getItersFromLine(buf, buf.numLinhaAnterior)
                    buf.insert(iterLineEnd,';')
                    #print("inseriu")

                    # Aplica Tag de Highlight
                    iterLineStart,iterLineEnd,strLinhaTexto = self.getItersFromLine(buf, buf.numLinhaAnterior)
                    buf.apply_tag(buf.tag_item, iterLineStart, iterLineEnd)
        # Por último
        buf.numLinhaAnterior = buf.numLinhaAtual

        # Testa se removeu todo o texto
        if (buf.get_text(buf.get_start_iter(),buf.get_end_iter(),False) == ''):
            buf.listCheck = []
            buf.numLinhaAnterior = 0
            buf.numLinhaAtual = 0



Builder = Gtk.Builder()
Builder.add_from_file(strGladeFullFilename)
Builder.connect_signals(Manipulador())
Window: Gtk.Window = Builder.get_object("main_window")
Window.show_all()
Gtk.main()
