        # -----------------------
        # Operações com o texto da Linha
        # -----------------------
        # https://www.nltk.org/api/nltk.tokenize.html
        listSep = WordPunctTokenizer().tokenize(strLinhaTexto)
        # print(listSep)

        listInd = list(WordPunctTokenizer().span_tokenize(strLinhaTexto))
        # print(listInd)

        # Remove todas as tags sempre e depois eu refaço
        buf.remove_all_tags(iterLineStart, iterLineEnd)


        # *Italic characters*
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_italic,'*',strLinhaTexto,numLinhaAtual)

        # _Italic characters_
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_italic,'_',strLinhaTexto,numLinhaAtual)

        # **bold characters**
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_bold,'**',strLinhaTexto,numLinhaAtual)

        # __bold characters__
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_bold,'__',strLinhaTexto,numLinhaAtual)

        # ~~strikethrough text~~
        self.aplicaTagEmOperadores(self.tvDiarioNotas.tag_strike,'~~',strLinhaTexto,numLinhaAtual)


        if '>' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_quote, iterLineStart, iterLineEnd)
        elif '####' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q4, iterLineStart, iterLineEnd)
        elif '###' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q3, iterLineStart, iterLineEnd)
        elif '##' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q2, iterLineStart, iterLineEnd)
        elif '#' in listSep:
            buf.apply_tag(self.tvDiarioNotas.tag_q1, iterLineStart, iterLineEnd)



    def aplicaTagEmOperadores(self,tag,strOperador,strLinhaTexto,numLinhaAtual):
        buf = self.tvDiarioNotas.textbuffer
        listSep = WordPunctTokenizer().tokenize(strLinhaTexto)
        listInd = list(WordPunctTokenizer().span_tokenize(strLinhaTexto))
        # https://stackoverflow.com/questions/176918/finding-the-index-of-an-item-in-a-list
        listOperador = [i for i, j in enumerate(listSep) if j == strOperador]
        #listOperador contem os indices de quando ** aparece
        # Preciso transformar em pares de indices
        llPares = []
        index = 0
        for i in listOperador:
            index = index + 1
            if (index % 2 == 0):
                llPares.append([listOperador[index-2],listOperador[index-1]])
        #Agora vou de par em par para fazer o apply tag
        # listPar[0] contem o indice da esquerda
        # listPar[1] contem o indice da direita
        for listPar in llPares:
            # Esses abaixo incluem os operadores
            numCharStart = listInd[listPar[0]][0]
            numCharEnd = listInd[listPar[1]][1]
            # Esses abaixo não incluem os operadores
            #numCharStart = listInd[listPar[0]][1]
            #numCharEnd = listInd[listPar[1]][0]
            # Agora preciso descobrir como pegar iter no char
            # print('Vai de {} até {}'.format(numCharStart,numCharEnd))
            iterPosStart = buf.get_iter_at_line(numLinhaAtual)
            # print("1")
            # print(iterPosStart.get_char())
            iterPosStart.forward_chars(numCharStart)
            # print("2")
            # print(iterPosStart.get_char())
            iterPosEnd = buf.get_iter_at_line(numLinhaAtual)
            # print("3")
            # print(iterPosEnd.get_char())
            iterPosEnd.forward_chars(numCharEnd)
            # print("4")
            # print(iterPosEnd.get_char())
            # Finalmente aplica a tag
            buf.apply_tag(tag, iterPosStart, iterPosEnd)

